﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrabalhoMarcos.Codigos.BASE;

namespace TrabalhoMarcos.Codigos.Login
{
    class loginDatabase
    {
        public loginDTO Logar(string login, string senha)
        {
            string script = @"SELECT * FROM login WHERE nm_login = @nm_login AND ds_senha = @ds_senha";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_login", login));
            parms.Add(new MySqlParameter("ds_senha", senha));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            loginDTO dto = null;
            if (reader.Read())
            {
                dto = new loginDTO();
                
                dto.login = reader.GetString("nm_login");
                dto.senha = reader.GetString("ds_senha");
            }

            reader.Close();
            return dto;
           
        }
        public int Sslvar(loginDTO dto)
        {
            string script = @"INSERT login(nm_login,ds_senha) 
                               VALUES (@nm_login,@ds_senha)";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            parms.Add(new MySqlParameter("nm_login", dto.login));
            parms.Add(new MySqlParameter("ds_senha", dto.senha));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }
    }
}
