﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrabalhoMarcos.Codigos.orcamento
{
    class orcamentoDTO
    {
        public int id { get; set; }
        public string descricao { get; set; }
        public decimal valor { get; set; }
        public int telefone { get; set; }
        public string forma { get; set; }
    }
}
