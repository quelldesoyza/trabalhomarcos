﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrabalhoMarcos.Codigos.BASE;

namespace TrabalhoMarcos.Codigos.orcamento
{
    class orcamentoDatabase
    {
        public int Salvar (orcamentoDTO dto )
        {
            string script = @"INSERT INTO orcamento(valor, descricao,forma) VALUES (@valor, @descricao,@forma)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_orcamento", dto.id));
            parms.Add(new MySqlParameter("valor", dto.valor));
            parms.Add(new MySqlParameter("descricao", dto.descricao));
            parms.Add(new MySqlParameter("forma", dto.forma));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);

        }
        public List<Clienteorcamento> Consultar(string cliente)
        {
            string script = @"SELECT * FROM consultar_orca WHERE nome like @nome";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nome", cliente + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<Clienteorcamento> lista = new List<Clienteorcamento>();
            while (reader.Read())
            {
                Clienteorcamento dto = new Clienteorcamento();
                dto.nome = reader.GetString("nome");
                dto.telefone = reader.GetInt32("telefone_resi");
                dto.valor = reader.GetDecimal("valor");
               

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
    }
}
