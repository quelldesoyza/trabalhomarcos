﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrabalhoMarcos.Codigos
{
    class carroDTO
    {
        public int id { get; set; }
        public string placa { get; set; }
        public string modelo { get; set; }
        public int ano { get; set; }
        public string cor { get; set; }
        public string proprietario { get; set; }
        public string obeserva { get; set; }


    }
}
