﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrabalhoMarcos.Codigos.BASE;

namespace TrabalhoMarcos.Codigos
{
    class carroDatabase
    {
        public int Salvar(carroDTO dto)
        {
            string script = @"INSERT INTO cadastro_do_carro (placa, modelo,ano, cor,proprietario, observa) values (@placa, @modelo, @ano,@cor,@proprietario,@observa)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("placa", dto.placa));
            parms.Add(new MySqlParameter("modelo", dto.modelo));
            parms.Add(new MySqlParameter("ano", dto.ano));
            parms.Add(new MySqlParameter("cor", dto.cor));
            parms.Add(new MySqlParameter("proprietario",dto.proprietario));
            parms.Add(new MySqlParameter("observa", dto.obeserva));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public List<carroDTO> Listar()
        {
            string script = @"SELECT * FROM cadastro_do_carro";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<carroDTO> Lista = new List<carroDTO>();
            while (reader.Read())
            {
                carroDTO dto = new carroDTO();
                dto.id = reader.GetInt32("id");
                dto.placa = reader.GetString("placa");
                dto.modelo = reader.GetString("modelo");
                dto.ano = reader.GetInt32("ano");
                dto.cor = reader.GetString("cor");
                dto.proprietario = reader.GetString("proprietario");
                dto.obeserva = reader.GetString("observa");

                Lista.Add(dto);
            }
            reader.Close();
            return Lista;

        }
       


    }
}
