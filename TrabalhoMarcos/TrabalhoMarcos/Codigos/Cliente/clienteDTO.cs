﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrabalhoMarcos.Codigos.Cliente
{
    class clienteDTO
    {
        public int id { get; set; }
        public string nome { get; set; }
        public int CPF { get; set; }
        public string sexo { get; set; }
        public DateTime dta_nacs { get; set; }
        public int cep { get; set; }
        public string cidade { get; set; }
        public int numero { get; set; }
        public string estado { get; set; }
        public string rua { get; set; }
        public string bairro { get; set; }
        public string referencia { get; set; }
        public string cpto { get; set; }
        public int tele_reside { get; set; }
        public int celular { get; set; }

    }
}
