﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrabalhoMarcos.Codigos.BASE;

namespace TrabalhoMarcos.Codigos.Cliente
{
    class clienteDatabase
    {
        public int Salvar(clienteDTO dto)
        {
            string script = @"INSERT INTO tb_cliente (nome,CPF,sexo,dta_nasc,rua,cep,cidade,numero,estado,bairro,referencia,cpto,telefone_resi,celular)
                                            values (@nome,@CPF,@sexo,@dta_nasc,@rua,@cep,@cidade,@numero,@estado,@bairro,@referencia,@cpto,@telefone_resi,celular)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nome", dto.nome));
            parms.Add(new MySqlParameter("CPF", dto.CPF));
            parms.Add(new MySqlParameter("sexo", dto.sexo));
            parms.Add(new MySqlParameter("dta_nasc", dto.dta_nacs));
            parms.Add(new MySqlParameter("rua",dto.rua));
            parms.Add(new MySqlParameter("cep", dto.cep));
            parms.Add(new MySqlParameter("cidade", dto.cidade));
            parms.Add(new MySqlParameter("numero", dto.numero));
            parms.Add(new MySqlParameter("estado", dto.estado));
            parms.Add(new MySqlParameter("bairro", dto.bairro));
            parms.Add(new MySqlParameter("referencia", dto.referencia));
            parms.Add(new MySqlParameter("cpto", dto.cpto));
            parms.Add(new MySqlParameter("telefone_resi", dto.tele_reside));
            parms.Add(new MySqlParameter("celular", dto.celular));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public List<clienteDTO> Listar()
        {
            string script = @"SELECT * FROM tb_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<clienteDTO> Lista = new List<clienteDTO>();
            while (reader.Read())
            {
                clienteDTO dto = new clienteDTO();
                dto.id = reader.GetInt32("id");
                dto.nome = reader.GetString("nome");
                dto.sexo = reader.GetString("sexo");
                dto.CPF = reader.GetInt32("CPF");
                dto.dta_nacs = reader.GetDateTime("dta_nasc");
                dto.cep = reader.GetInt32("cep");
                dto.cidade = reader.GetString("numero");
                dto.numero = reader.GetInt32("estado");
                dto.bairro = reader.GetString("bairro");
                dto.referencia = reader.GetString("referencia");
                dto.cpto = reader.GetString("cpto");
                dto.tele_reside = reader.GetInt32("telefone_resi");
                dto.celular = reader.GetInt32("celular");
                Lista.Add(dto);
            }
            reader.Close();
            return Lista;

        }

    }
}
