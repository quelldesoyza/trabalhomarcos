﻿namespace TrabalhoMarcos
{
    partial class Cadastro_de_cliente
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblnome = new System.Windows.Forms.Label();
            this.lblcpf = new System.Windows.Forms.Label();
            this.lblsexo = new System.Windows.Forms.Label();
            this.lblendereço = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lblcidade = new System.Windows.Forms.Label();
            this.lblcontado = new System.Windows.Forms.Label();
            this.lblrua = new System.Windows.Forms.Label();
            this.lblbairro = new System.Windows.Forms.Label();
            this.lbltelefone = new System.Windows.Forms.Label();
            this.lblcelular = new System.Windows.Forms.Label();
            this.txtnome = new System.Windows.Forms.TextBox();
            this.txtcpf = new System.Windows.Forms.TextBox();
            this.txtbairro = new System.Windows.Forms.TextBox();
            this.txtrua = new System.Windows.Forms.TextBox();
            this.txtcelular = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.txttelefone = new System.Windows.Forms.TextBox();
            this.txtcidade = new System.Windows.Forms.TextBox();
            this.cbosexo = new System.Windows.Forms.ComboBox();
            this.lblestado = new System.Windows.Forms.Label();
            this.lblnúmero = new System.Windows.Forms.Label();
            this.lblcep = new System.Windows.Forms.Label();
            this.lblcomplemento = new System.Windows.Forms.Label();
            this.lbldara_de_nascimeto = new System.Windows.Forms.Label();
            this.lblreferêcia = new System.Windows.Forms.Label();
            this.txtcep = new System.Windows.Forms.TextBox();
            this.txtreferência = new System.Windows.Forms.TextBox();
            this.txtcomplemento = new System.Windows.Forms.TextBox();
            this.cboestado = new System.Windows.Forms.ComboBox();
            this.btncancelar = new System.Windows.Forms.Button();
            this.btnsalvar = new System.Windows.Forms.Button();
            this.btnfechar = new System.Windows.Forms.Button();
            this.btnalterar = new System.Windows.Forms.Button();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.SuspendLayout();
            // 
            // lblnome
            // 
            this.lblnome.AutoSize = true;
            this.lblnome.Location = new System.Drawing.Point(12, 57);
            this.lblnome.Name = "lblnome";
            this.lblnome.Size = new System.Drawing.Size(35, 13);
            this.lblnome.TabIndex = 2;
            this.lblnome.Text = "Nome";
            this.lblnome.Click += new System.EventHandler(this.label3_Click_1);
            // 
            // lblcpf
            // 
            this.lblcpf.AutoSize = true;
            this.lblcpf.Location = new System.Drawing.Point(11, 81);
            this.lblcpf.Name = "lblcpf";
            this.lblcpf.Size = new System.Drawing.Size(27, 13);
            this.lblcpf.TabIndex = 3;
            this.lblcpf.Text = "CPF";
            // 
            // lblsexo
            // 
            this.lblsexo.AutoSize = true;
            this.lblsexo.Location = new System.Drawing.Point(11, 106);
            this.lblsexo.Name = "lblsexo";
            this.lblsexo.Size = new System.Drawing.Size(34, 13);
            this.lblsexo.TabIndex = 4;
            this.lblsexo.Text = "Sexo ";
            // 
            // lblendereço
            // 
            this.lblendereço.AutoSize = true;
            this.lblendereço.Location = new System.Drawing.Point(12, 142);
            this.lblendereço.Name = "lblendereço";
            this.lblendereço.Size = new System.Drawing.Size(53, 13);
            this.lblendereço.TabIndex = 5;
            this.lblendereço.Text = "Endereço";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(86, 246);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(0, 13);
            this.label7.TabIndex = 6;
            // 
            // lblcidade
            // 
            this.lblcidade.AutoSize = true;
            this.lblcidade.Location = new System.Drawing.Point(11, 230);
            this.lblcidade.Name = "lblcidade";
            this.lblcidade.Size = new System.Drawing.Size(43, 13);
            this.lblcidade.TabIndex = 7;
            this.lblcidade.Text = "Cidade ";
            // 
            // lblcontado
            // 
            this.lblcontado.AutoSize = true;
            this.lblcontado.Location = new System.Drawing.Point(11, 261);
            this.lblcontado.Name = "lblcontado";
            this.lblcontado.Size = new System.Drawing.Size(47, 13);
            this.lblcontado.TabIndex = 8;
            this.lblcontado.Text = "Contado";
            // 
            // lblrua
            // 
            this.lblrua.AutoSize = true;
            this.lblrua.Location = new System.Drawing.Point(12, 167);
            this.lblrua.Name = "lblrua";
            this.lblrua.Size = new System.Drawing.Size(30, 13);
            this.lblrua.TabIndex = 9;
            this.lblrua.Text = "Rua ";
            // 
            // lblbairro
            // 
            this.lblbairro.AutoSize = true;
            this.lblbairro.Location = new System.Drawing.Point(11, 195);
            this.lblbairro.Name = "lblbairro";
            this.lblbairro.Size = new System.Drawing.Size(34, 13);
            this.lblbairro.TabIndex = 10;
            this.lblbairro.Text = "Bairro";
            // 
            // lbltelefone
            // 
            this.lbltelefone.AutoSize = true;
            this.lbltelefone.Location = new System.Drawing.Point(8, 285);
            this.lbltelefone.Name = "lbltelefone";
            this.lbltelefone.Size = new System.Drawing.Size(52, 13);
            this.lbltelefone.TabIndex = 11;
            this.lbltelefone.Text = "Telefone ";
            // 
            // lblcelular
            // 
            this.lblcelular.AutoSize = true;
            this.lblcelular.Location = new System.Drawing.Point(426, 292);
            this.lblcelular.Name = "lblcelular";
            this.lblcelular.Size = new System.Drawing.Size(39, 13);
            this.lblcelular.TabIndex = 12;
            this.lblcelular.Text = "Celular";
            // 
            // txtnome
            // 
            this.txtnome.Location = new System.Drawing.Point(53, 54);
            this.txtnome.Name = "txtnome";
            this.txtnome.Size = new System.Drawing.Size(230, 20);
            this.txtnome.TabIndex = 15;
            // 
            // txtcpf
            // 
            this.txtcpf.Location = new System.Drawing.Point(53, 80);
            this.txtcpf.Name = "txtcpf";
            this.txtcpf.Size = new System.Drawing.Size(100, 20);
            this.txtcpf.TabIndex = 16;
            // 
            // txtbairro
            // 
            this.txtbairro.Location = new System.Drawing.Point(65, 198);
            this.txtbairro.Name = "txtbairro";
            this.txtbairro.Size = new System.Drawing.Size(173, 20);
            this.txtbairro.TabIndex = 18;
            // 
            // txtrua
            // 
            this.txtrua.Location = new System.Drawing.Point(53, 164);
            this.txtrua.Name = "txtrua";
            this.txtrua.Size = new System.Drawing.Size(230, 20);
            this.txtrua.TabIndex = 19;
            // 
            // txtcelular
            // 
            this.txtcelular.Location = new System.Drawing.Point(471, 288);
            this.txtcelular.Name = "txtcelular";
            this.txtcelular.Size = new System.Drawing.Size(100, 20);
            this.txtcelular.TabIndex = 20;
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(358, 168);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(50, 20);
            this.textBox8.TabIndex = 22;
            // 
            // txttelefone
            // 
            this.txttelefone.Location = new System.Drawing.Point(80, 288);
            this.txttelefone.Name = "txttelefone";
            this.txttelefone.Size = new System.Drawing.Size(100, 20);
            this.txttelefone.TabIndex = 23;
            // 
            // txtcidade
            // 
            this.txtcidade.Location = new System.Drawing.Point(65, 227);
            this.txtcidade.Name = "txtcidade";
            this.txtcidade.Size = new System.Drawing.Size(131, 20);
            this.txtcidade.TabIndex = 25;
            // 
            // cbosexo
            // 
            this.cbosexo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbosexo.FormattingEnabled = true;
            this.cbosexo.Items.AddRange(new object[] {
            "Masculino",
            "Feminino"});
            this.cbosexo.Location = new System.Drawing.Point(53, 108);
            this.cbosexo.Name = "cbosexo";
            this.cbosexo.Size = new System.Drawing.Size(121, 21);
            this.cbosexo.TabIndex = 26;
            // 
            // lblestado
            // 
            this.lblestado.AutoSize = true;
            this.lblestado.Location = new System.Drawing.Point(450, 199);
            this.lblestado.Name = "lblestado";
            this.lblestado.Size = new System.Drawing.Size(40, 13);
            this.lblestado.TabIndex = 29;
            this.lblestado.Text = "Estado";
            // 
            // lblnúmero
            // 
            this.lblnúmero.AutoSize = true;
            this.lblnúmero.Location = new System.Drawing.Point(308, 171);
            this.lblnúmero.Name = "lblnúmero";
            this.lblnúmero.Size = new System.Drawing.Size(44, 13);
            this.lblnúmero.TabIndex = 30;
            this.lblnúmero.Text = "Número";
            // 
            // lblcep
            // 
            this.lblcep.AutoSize = true;
            this.lblcep.Location = new System.Drawing.Point(315, 201);
            this.lblcep.Name = "lblcep";
            this.lblcep.Size = new System.Drawing.Size(28, 13);
            this.lblcep.TabIndex = 31;
            this.lblcep.Text = "CEP";
            // 
            // lblcomplemento
            // 
            this.lblcomplemento.AutoSize = true;
            this.lblcomplemento.Location = new System.Drawing.Point(426, 174);
            this.lblcomplemento.Name = "lblcomplemento";
            this.lblcomplemento.Size = new System.Drawing.Size(71, 13);
            this.lblcomplemento.TabIndex = 32;
            this.lblcomplemento.Text = "Complemento";
            // 
            // lbldara_de_nascimeto
            // 
            this.lbldara_de_nascimeto.AutoSize = true;
            this.lbldara_de_nascimeto.Location = new System.Drawing.Point(289, 61);
            this.lbldara_de_nascimeto.Name = "lbldara_de_nascimeto";
            this.lbldara_de_nascimeto.Size = new System.Drawing.Size(99, 13);
            this.lbldara_de_nascimeto.TabIndex = 33;
            this.lbldara_de_nascimeto.Text = "Dta de nascimento ";
            // 
            // lblreferêcia
            // 
            this.lblreferêcia.AutoSize = true;
            this.lblreferêcia.Location = new System.Drawing.Point(263, 234);
            this.lblreferêcia.Name = "lblreferêcia";
            this.lblreferêcia.Size = new System.Drawing.Size(59, 13);
            this.lblreferêcia.TabIndex = 34;
            this.lblreferêcia.Text = "Referência";
            // 
            // txtcep
            // 
            this.txtcep.Location = new System.Drawing.Point(349, 198);
            this.txtcep.Name = "txtcep";
            this.txtcep.Size = new System.Drawing.Size(95, 20);
            this.txtcep.TabIndex = 40;
            // 
            // txtreferência
            // 
            this.txtreferência.Location = new System.Drawing.Point(322, 234);
            this.txtreferência.Name = "txtreferência";
            this.txtreferência.Size = new System.Drawing.Size(213, 20);
            this.txtreferência.TabIndex = 42;
            // 
            // txtcomplemento
            // 
            this.txtcomplemento.Location = new System.Drawing.Point(495, 174);
            this.txtcomplemento.Name = "txtcomplemento";
            this.txtcomplemento.Size = new System.Drawing.Size(100, 20);
            this.txtcomplemento.TabIndex = 43;
            this.txtcomplemento.TextChanged += new System.EventHandler(this.txtcomplemento_TextChanged);
            // 
            // cboestado
            // 
            this.cboestado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboestado.FormattingEnabled = true;
            this.cboestado.Items.AddRange(new object[] {
            "AC",
            "AL",
            "AP",
            "AM",
            "BA",
            "CE",
            "DF",
            "ES",
            "GO",
            "MA",
            "MT",
            "MS",
            "MG",
            "PA",
            "PB",
            "PR",
            "PE",
            "PI",
            "RJ",
            "RN",
            "RS",
            "RO",
            "RR",
            "SC",
            "SP",
            "SE",
            "TO"});
            this.cboestado.Location = new System.Drawing.Point(495, 201);
            this.cboestado.Name = "cboestado";
            this.cboestado.Size = new System.Drawing.Size(62, 21);
            this.cboestado.TabIndex = 44;
            // 
            // btncancelar
            // 
            this.btncancelar.Location = new System.Drawing.Point(121, 373);
            this.btncancelar.Name = "btncancelar";
            this.btncancelar.Size = new System.Drawing.Size(75, 23);
            this.btncancelar.TabIndex = 46;
            this.btncancelar.Text = "Cancelar";
            this.btncancelar.UseVisualStyleBackColor = true;
            // 
            // btnsalvar
            // 
            this.btnsalvar.Location = new System.Drawing.Point(333, 373);
            this.btnsalvar.Name = "btnsalvar";
            this.btnsalvar.Size = new System.Drawing.Size(75, 23);
            this.btnsalvar.TabIndex = 47;
            this.btnsalvar.Text = "Salvar";
            this.btnsalvar.UseVisualStyleBackColor = true;
            this.btnsalvar.Click += new System.EventHandler(this.btnsalvar_Click);
            // 
            // btnfechar
            // 
            this.btnfechar.Location = new System.Drawing.Point(437, 373);
            this.btnfechar.Name = "btnfechar";
            this.btnfechar.Size = new System.Drawing.Size(75, 23);
            this.btnfechar.TabIndex = 48;
            this.btnfechar.Text = "Fechar";
            this.btnfechar.UseVisualStyleBackColor = true;
            this.btnfechar.Click += new System.EventHandler(this.btnfechar_Click);
            // 
            // btnalterar
            // 
            this.btnalterar.Location = new System.Drawing.Point(24, 373);
            this.btnalterar.Name = "btnalterar";
            this.btnalterar.Size = new System.Drawing.Size(75, 23);
            this.btnalterar.TabIndex = 45;
            this.btnalterar.Text = "Alterar";
            this.btnalterar.UseVisualStyleBackColor = true;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(388, 61);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(223, 20);
            this.dateTimePicker1.TabIndex = 49;
            this.dateTimePicker1.Value = new System.DateTime(2018, 9, 8, 0, 0, 0, 0);
            // 
            // Cadastro_de_cliente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(623, 401);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.btnfechar);
            this.Controls.Add(this.btnsalvar);
            this.Controls.Add(this.btncancelar);
            this.Controls.Add(this.btnalterar);
            this.Controls.Add(this.cboestado);
            this.Controls.Add(this.txtcomplemento);
            this.Controls.Add(this.txtreferência);
            this.Controls.Add(this.txtcep);
            this.Controls.Add(this.lblreferêcia);
            this.Controls.Add(this.lbldara_de_nascimeto);
            this.Controls.Add(this.lblcomplemento);
            this.Controls.Add(this.lblcep);
            this.Controls.Add(this.lblnúmero);
            this.Controls.Add(this.lblestado);
            this.Controls.Add(this.cbosexo);
            this.Controls.Add(this.txtcidade);
            this.Controls.Add(this.txttelefone);
            this.Controls.Add(this.textBox8);
            this.Controls.Add(this.txtcelular);
            this.Controls.Add(this.txtrua);
            this.Controls.Add(this.txtbairro);
            this.Controls.Add(this.txtcpf);
            this.Controls.Add(this.txtnome);
            this.Controls.Add(this.lblcelular);
            this.Controls.Add(this.lbltelefone);
            this.Controls.Add(this.lblbairro);
            this.Controls.Add(this.lblrua);
            this.Controls.Add(this.lblcontado);
            this.Controls.Add(this.lblcidade);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.lblendereço);
            this.Controls.Add(this.lblsexo);
            this.Controls.Add(this.lblcpf);
            this.Controls.Add(this.lblnome);
            this.Name = "Cadastro_de_cliente";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblnome;
        private System.Windows.Forms.Label lblcpf;
        private System.Windows.Forms.Label lblsexo;
        private System.Windows.Forms.Label lblendereço;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblcidade;
        private System.Windows.Forms.Label lblcontado;
        private System.Windows.Forms.Label lblrua;
        private System.Windows.Forms.Label lblbairro;
        private System.Windows.Forms.Label lbltelefone;
        private System.Windows.Forms.Label lblcelular;
        private System.Windows.Forms.TextBox txtnome;
        private System.Windows.Forms.TextBox txtcpf;
        private System.Windows.Forms.TextBox txtbairro;
        private System.Windows.Forms.TextBox txtrua;
        private System.Windows.Forms.TextBox txtcelular;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox txttelefone;
        private System.Windows.Forms.TextBox txtcidade;
        private System.Windows.Forms.ComboBox cbosexo;
        private System.Windows.Forms.Label lblestado;
        private System.Windows.Forms.Label lblnúmero;
        private System.Windows.Forms.Label lblcep;
        private System.Windows.Forms.Label lblcomplemento;
        private System.Windows.Forms.Label lbldara_de_nascimeto;
        private System.Windows.Forms.Label lblreferêcia;
        private System.Windows.Forms.TextBox txtcep;
        private System.Windows.Forms.TextBox txtreferência;
        private System.Windows.Forms.TextBox txtcomplemento;
        private System.Windows.Forms.ComboBox cboestado;
        private System.Windows.Forms.Button btncancelar;
        private System.Windows.Forms.Button btnsalvar;
        private System.Windows.Forms.Button btnfechar;
        private System.Windows.Forms.Button btnalterar;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
    }
}

