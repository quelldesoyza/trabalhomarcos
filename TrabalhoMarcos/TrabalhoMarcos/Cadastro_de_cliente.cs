﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TrabalhoMarcos.Codigos.Cliente;
using TrabalhoMarcos.Telas;

namespace TrabalhoMarcos
{
    public partial class Cadastro_de_cliente : Form
    {
        public Cadastro_de_cliente()
        {
            InitializeComponent();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click_1(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btnsalvar_Click(object sender, EventArgs e)
        {
            clienteDTO dto = new clienteDTO();
            dto.nome = txtnome.Text;
            dto.CPF = Convert.ToInt32(txtcpf.Text);
            dto.sexo = cbosexo.Text;
            dto.dta_nacs = Convert.ToDateTime(dateTimePicker1.Text);
            dto.rua = txtrua.Text;
            dto.bairro = txtbairro.Text;
            dto.cidade = txtcidade.Text;
            dto.numero = Convert.ToInt32(textBox8.Text);
            dto.cpto = txtcomplemento.Text;
            dto.cep = Convert.ToInt32(txtcep.Text);
            dto.estado = cboestado.Text;
            dto.referencia = txtreferência.Text;
            dto.celular = Convert.ToInt32(txtcelular.Text);
            dto.tele_reside = Convert.ToInt32(txttelefone.Text);

            clienteBusiness business = new clienteBusiness();
            business.Salvar(dto);


            MessageBox.Show("Cadastro realizado com sucesso");
            Login tela = new Login();
            tela.Show();
            this.Close();
        }

        private void txtcomplemento_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnfechar_Click(object sender, EventArgs e)
        {
            Tela_inicio tele = new Tela_inicio();
            tele.Show(); this.Hide();
        }
    }
}
