﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TrabalhoMarcos.Codigos.Login;

namespace TrabalhoMarcos.Telas
{
    public partial class Criar_Login : Form
    {
        public Criar_Login()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            loginDTO dto = new loginDTO();
            dto.login = textBox1.Text;
            dto.senha = textBox2.Text;
            loginBusiness business = new loginBusiness();
            business.Salvar(dto);

            MessageBox.Show("Login realizado com sucesso");
            Login tela = new Login();
            tela.Show();
        }
    }
}
