﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TrabalhoMarcos.Codigos.orcamento;

namespace TrabalhoMarcos.Telas
{
    public partial class Consultar_orcamen : Form
    {
        public Consultar_orcamen()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            orcamnetoBusiness business = new orcamnetoBusiness();
            List<Clienteorcamento> lista = business.Consultar(textBox1.Text);

            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.DataSource = lista;
        }

        private void label2_Click(object sender, EventArgs e)
        {
            Tela_inicio tela = new Tela_inicio();
            tela.Show();
            this.Hide();
        }
    }
}
