﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TrabalhoMarcos.Codigos.Cliente;
using TrabalhoMarcos.Codigos.Login;

namespace TrabalhoMarcos.Telas
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Criar_Login tela = new Criar_Login();
            tela.Show();
            this.Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            loginBusiness business = new loginBusiness();
            loginDTO cli = business.Logar(textBox2.Text, textBox1.Text);
            if (cli != null)
            {
                usersession.usuariologado = cli;

                Tela_inicio tela = new Tela_inicio();
                tela.Show();
                this.Hide();
            }
            else
            {
                MessageBox.Show("Nome ou senha estão incorretos");
            }
        }
    }
}
