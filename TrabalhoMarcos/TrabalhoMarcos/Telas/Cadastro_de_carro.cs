﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TrabalhoMarcos.Codigos;

namespace TrabalhoMarcos.Telas
{
    public partial class Cadastro_de_carro : Form
    {
        public Cadastro_de_carro()
        {
            InitializeComponent();
        }

        private void txtobservções_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnsalvar_Click(object sender, EventArgs e)
        {
            carroDTO dto = new carroDTO();
            dto.placa = txtplaca.Text;
            dto.proprietario = txtproprietário.Text;
            dto.modelo = txtmodelo.Text;
            dto.cor = txtcor.Text;
            dto.ano =Convert.ToInt32(cboano_de_fabricação.Text);
            dto.obeserva = txtobservções.Text;

            carroBusiness business = new carroBusiness();
            business.Salvar(dto);

            MessageBox.Show("Cadastro de carro realizado com sucesso");
            Tela_inicio tela = new Tela_inicio();
            tela.Show();
            this.Close();
        }

        private void label1_Click(object sender, EventArgs e)
        {
            Tela_inicio tela = new Tela_inicio();
            tela.Show();
            this.Hide();
        }
    }
}
