﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TrabalhoMarcos.Telas
{
    public partial class Tela_inicio : Form
    {
        public Tela_inicio()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Cadastro_de_cliente tela = new Cadastro_de_cliente();
            tela.ShowDialog();
            this.Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {

            Consultar_orcamen tela = new Consultar_orcamen();
            tela.ShowDialog();
            this.Hide();
        }

        private void button3_Click(object sender, EventArgs e)
        {

            Cadastro_de_carro tela = new Cadastro_de_carro();
            tela.ShowDialog();
            this.Hide();
        }

        private void button4_Click(object sender, EventArgs e)
        {

            orçamentos tela = new orçamentos();
            tela.ShowDialog();
            this.Hide();
        }

        private void button5_Click(object sender, EventArgs e)
        {

        }
    }
}
