﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TrabalhoMarcos.Codigos.orcamento;

namespace TrabalhoMarcos.Telas
{
    public partial class orçamentos : Form
    {
        public orçamentos()
        {
            InitializeComponent();
        }

        private void sal_Click(object sender, EventArgs e)
        {
            if (sender == null)
            {
                throw new ArgumentNullException(nameof(sender));
            }

            orcamentoDTO dto = new orcamentoDTO();
            dto.forma= comboBox1.Text;
            dto.descricao = textBox3.Text;
            dto.valor = Convert.ToDecimal(textBox1.Text);
            dto.telefone =Convert.ToInt32(textBox2.Text);

            orcamnetoBusiness business = new orcamnetoBusiness();
            business.Salvar(dto);

            MessageBox.Show("Produto salvo com sucesso.");
           
        }

        private void label5_Click(object sender, EventArgs e)
        {
            Tela_inicio tela = new Tela_inicio();
            tela.Show();
            this.Hide();
        }
    }
}
