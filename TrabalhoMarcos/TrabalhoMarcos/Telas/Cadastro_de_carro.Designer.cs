﻿namespace TrabalhoMarcos.Telas
{
    partial class Cadastro_de_carro
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtobservções = new System.Windows.Forms.TextBox();
            this.txtplaca = new System.Windows.Forms.TextBox();
            this.txtproprietário = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.cboano_de_fabricação = new System.Windows.Forms.ComboBox();
            this.txtcor = new System.Windows.Forms.TextBox();
            this.btnsalvar = new System.Windows.Forms.Button();
            this.btnfechar = new System.Windows.Forms.Button();
            this.txtmodelo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Placa";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(5, 75);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "proprietário";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(4, 108);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Modelo";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(184, 213);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(70, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Observações";
            // 
            // txtobservções
            // 
            this.txtobservções.Location = new System.Drawing.Point(12, 239);
            this.txtobservções.Multiline = true;
            this.txtobservções.Name = "txtobservções";
            this.txtobservções.Size = new System.Drawing.Size(423, 96);
            this.txtobservções.TabIndex = 5;
            this.txtobservções.TextChanged += new System.EventHandler(this.txtobservções_TextChanged);
            // 
            // txtplaca
            // 
            this.txtplaca.Location = new System.Drawing.Point(52, 44);
            this.txtplaca.Multiline = true;
            this.txtplaca.Name = "txtplaca";
            this.txtplaca.Size = new System.Drawing.Size(125, 27);
            this.txtplaca.TabIndex = 6;
            // 
            // txtproprietário
            // 
            this.txtproprietário.Location = new System.Drawing.Point(70, 77);
            this.txtproprietário.Name = "txtproprietário";
            this.txtproprietário.Size = new System.Drawing.Size(207, 20);
            this.txtproprietário.TabIndex = 7;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(310, 84);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(94, 13);
            this.label8.TabIndex = 10;
            this.label8.Text = "Ano de fabricaçâo";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(361, 58);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(23, 13);
            this.label9.TabIndex = 11;
            this.label9.Text = "Cor";
            // 
            // cboano_de_fabricação
            // 
            this.cboano_de_fabricação.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboano_de_fabricação.FormattingEnabled = true;
            this.cboano_de_fabricação.Items.AddRange(new object[] {
            "2018",
            "2017",
            "2016",
            "2015",
            "2014",
            "2013",
            "2012",
            "2011",
            "2010",
            "2009",
            "2008",
            "2007",
            "2006",
            "2005",
            "2004",
            "2003",
            "2002",
            "2001",
            "2000"});
            this.cboano_de_fabricação.Location = new System.Drawing.Point(414, 84);
            this.cboano_de_fabricação.Name = "cboano_de_fabricação";
            this.cboano_de_fabricação.Size = new System.Drawing.Size(75, 21);
            this.cboano_de_fabricação.TabIndex = 12;
            // 
            // txtcor
            // 
            this.txtcor.Location = new System.Drawing.Point(390, 58);
            this.txtcor.Name = "txtcor";
            this.txtcor.Size = new System.Drawing.Size(99, 20);
            this.txtcor.TabIndex = 14;
            // 
            // btnsalvar
            // 
            this.btnsalvar.Location = new System.Drawing.Point(38, 415);
            this.btnsalvar.Name = "btnsalvar";
            this.btnsalvar.Size = new System.Drawing.Size(63, 23);
            this.btnsalvar.TabIndex = 20;
            this.btnsalvar.Text = "Salvar";
            this.btnsalvar.UseVisualStyleBackColor = true;
            this.btnsalvar.Click += new System.EventHandler(this.btnsalvar_Click);
            // 
            // btnfechar
            // 
            this.btnfechar.Location = new System.Drawing.Point(450, 415);
            this.btnfechar.Name = "btnfechar";
            this.btnfechar.Size = new System.Drawing.Size(65, 23);
            this.btnfechar.TabIndex = 21;
            this.btnfechar.Text = "Fechar";
            this.btnfechar.UseVisualStyleBackColor = true;
            // 
            // txtmodelo
            // 
            this.txtmodelo.Location = new System.Drawing.Point(52, 108);
            this.txtmodelo.Name = "txtmodelo";
            this.txtmodelo.Size = new System.Drawing.Size(125, 20);
            this.txtmodelo.TabIndex = 22;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(390, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 23;
            this.label1.Text = "Voltar";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // Cadastro_de_carro
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(549, 450);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtmodelo);
            this.Controls.Add(this.btnfechar);
            this.Controls.Add(this.btnsalvar);
            this.Controls.Add(this.txtcor);
            this.Controls.Add(this.cboano_de_fabricação);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtproprietário);
            this.Controls.Add(this.txtplaca);
            this.Controls.Add(this.txtobservções);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Name = "Cadastro_de_carro";
            this.Text = "Cadastro_de_carro.cs";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtobservções;
        private System.Windows.Forms.TextBox txtplaca;
        private System.Windows.Forms.TextBox txtproprietário;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cboano_de_fabricação;
        private System.Windows.Forms.TextBox txtcor;
        private System.Windows.Forms.Button btnsalvar;
        private System.Windows.Forms.Button btnfechar;
        private System.Windows.Forms.TextBox txtmodelo;
        private System.Windows.Forms.Label label1;
    }
}